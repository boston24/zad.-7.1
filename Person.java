package com.example.validForm;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class Person {
    @NotNull(message = "Name is required")
    @Size(min = 2, message = "Name should be start at least two characters")
    private String name;

    @NotNull(message = "Age is required")
    @Min(value = 1, message = "Age must be at least zero")
    private int age;

    @NotNull(message = "Postal code is required")
    @Pattern(regexp = "[0-9]{2}[-][0-9]{3}", message = "Error")
    private String postalCode;

    @NotNull(message = "Income is required")
    @Min(value = 2000, message = "Income must be between 2000 and 3000")
    @Max(value = 3000, message = "Income must be between 2000 and 3000")
    private int income;

    @Pattern(regexp = "true", message = "Check the box dude")
    private String agreement = "false";



}
